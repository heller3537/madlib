// Aaron Heller
//Lab Exercise 3: Mad Lib

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;


void Print(string* collection, ostream &out)
{
	out << "One day my " << collection[0] << "friend and I decided to go to a " << collection[1] << "game in " << collection[2] << endl;
	out << "We really wanted to see " << collection[3] << "play." << endl;
	out << "So we " << collection[4] << "in the " << collection[5] << "and headed down to the " << collection[6] << "and bought some" << collection[7] << endl;
	out << "We watched the game and it was " << collection[8] << endl;
	out << "We ate some " << collection[9] << "and drank some " << collection[10] << endl;
	out << "We had a " << collection[11] << "time, and can't wait to go again." << endl;
}

int main()
{
	
	const int NUM_WORDS = 12;
	string arrCollection[NUM_WORDS];
	string words[NUM_WORDS] = { "Enter an adjective (Describing word): ", "Enter a sport: ", "Enter a city name: ", "Enter a person: ",
		"Enter an action verb (past tense): ", "Enter a vehicle: ", "Enter a place: ", "Enter a noun (thing, plural): ", "Enter an adjective (Describing word): ",
		"Enter a food (plural): ", "Enter a liquid: ", "Enter an adjective (describing word): " };
	
	for (int i = 0; i < 12; i++)
	{
		cout << words[i];
		getline(cin, arrCollection[i]);
	}

	Print(arrCollection, cout);

	string filepath = "C:\\temp\\MadLib.txt";

	char input;
	cout << "Would you like to save the text? (y/n)";
	cin >> input;
	if (input == 'Y' || input == 'y')
	{
		ofstream fout(filepath);
		Print(arrCollection, fout);
		fout.close();
	}



	_getch();
	return 0;
}
